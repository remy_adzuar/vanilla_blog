#Create path between public/css and html files

import os
from bs4 import BeautifulSoup

## Definition of css files
### TODO : export in a config file
css_files = {
    "global": "./public/css/global.css",
    "grid": "./public/css/grid.css",
    "index": "./public/css/index.css",
    "nav": "./public/css/nav.css"
}

html_files = {
    "index": "./public/index.html",
    "legal_mention": "./public/legal_mention.html",
    "creation_blog": "./public/memos/web/creation_blog.html",
    "home_memos": "./public/memos/web/home_memos.html",
    "home_posts": "./public/posts/home_posts.html",
    "snake": "./public/posts/snake.html"
}


def create_linker(file1, file2):
    # Return the relative path between file1 and file2

    abs_path_file1 = os.path.abspath(file1)
    abs_path_file2 = os.path.abspath(file2)


    if not os.path.exists(abs_path_file1):
        os.makedirs(abs_path_file1)
    if not os.path.exists(abs_path_file2):
        os.makedirs(abs_path_file2)
    relative_path = os.path.relpath(abs_path_file2, os.path.dirname(abs_path_file1))
    print("Link calculate between " + abs_path_file1 + " and " + abs_path_file2)
    print("Relative Path = " + relative_path)
    return relative_path

assert create_linker(html_files["index"], css_files["global"]) == "css/global.css"

def link_css_files():
    # Create the path between css files and html files
    for css_key in css_files:
        for html_key in html_files:
            create_linker(html_files[html_key], css_files[css_key])

### TODO : add test
link_css_files()

### CREATE nav tag in index.html

def add_a_css_link(html_file, relative_path):
    with open(html_file, "r") as f:
        soup = BeautifulSoup(f, "html.parser")
        nav = soup.find('html')
        nav = nav.find('head')
        link = soup.new_tag("link", href=relative_path, rel="stylesheet")
        nav.append(link)

    with open(html_file, "w") as f:
        f.write(soup.prettify())

### TODO : add test
add_a_css_link(html_files["index"], create_linker(html_files["index"], css_files["grid"]))