// Hexa To Binary :

function createHexa() {
  let hexaNumber = document.getElementById("hexa-number");
  let hexaWordNumber = document.getElementById("hexa-word-number");

  console.log(hexaWordNumber.value);

  let result = "";
  for (let i = 0; i < hexaWordNumber.value; i++) {
    result += createOneWordHexa();
  }

  hexaNumber.innerHTML = result;
}

function createOneWordHexa() {
  return Math.floor(Math.random() * 16)
    .toString(16)
    .toUpperCase();
}

function readBinary() {
  let binaryInput = document.getElementById("binary-entry");
  let hexaNumber = document.getElementById("hexa-number");

  let inputSanitized = binaryInput.value.replaceAll(" ", "");
  let hexaInput = parseInt(inputSanitized, 2).toString(16).toUpperCase();

  if (hexaInput === hexaNumber.innerHTML) {
    hexaNumber.innerHTML = "Gagné";
  } else {
    hexaNumber.innerHTML =
      "Perdu => " + parseInt(hexaNumber.innerHTML, 16).toString(2);
  }
}

function hexaWordDisplay() {
  let displaySpan = document.getElementById("hexa-word-number-display");
  let wordNumber = document.getElementById("hexa-word-number");

  displaySpan.innerHTML = wordNumber.value.toString();
}
hexaWordDisplay(); // display it when loading the page

// Binary To Hexa

function createBinary() {
  let binaryNumber = document.getElementById("binary-number");
  let binaryBitsNumber = document.getElementById("binary-word-number");

  let result = "";
  for (let i = 0; i < binaryBitsNumber.value; i += 4) {
    result += createOneHexaWordToBinary();
    result += " ";
  }

  binaryNumber.innerHTML = result;
}

function createOneHexaWordToBinary() {
  return Math.floor(Math.random() * 16)
    .toString(2)
    .padStart(4, 0)
    .toUpperCase();
}

function readHexa() {
  let hexaInput = document.getElementById("hexa-entry");
  let binaryNumberNode = document.getElementById("binary-number");

  let inputSanitized = hexaInput.value.replaceAll(" ", "");
  let binaryInput = parseInt(inputSanitized, 16);

  let binaryNumber = parseInt(
    binaryNumberNode.innerHTML.toString().replaceAll(" ", ""),
    2
  );

  if (binaryInput === binaryNumber) {
    binaryNumberNode.innerHTML = "Gagné";
  } else {
    console.log(binaryNumber);
    console.log();
    binaryNumberNode.innerHTML =
      "Perdu => " + binaryNumber.toString(16).toUpperCase();
  }
}
