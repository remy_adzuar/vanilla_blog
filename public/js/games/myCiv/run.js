console.log("MyCiv starting....");

const canvas = document.getElementById("game-map");
const ctx = canvas.getContext("2d");
const canvasBorder = canvas.getBoundingClientRect();

const mousePos = {
  x: 0,
  y: 0,
};

console.log(canvas);
console.log(ctx);

function updateGame() {
  console.log(ctx);
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawGame() {}

canvas.addEventListener("click", function (evt) {
  console.log(evt);
  mousePos.x = (evt.clientX - canvasBorder.left) | 0; // parse float to int
  mousePos.y = (evt.clientY - canvasBorder.top) | 0;
  console.log(mousePos);
});

while (true) {
  updateGame();
  drawGame();
  break;
}
