let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

ctx.fillStyle = "black";
ctx.fillRect(0, 0, canvas.width, canvas.height);

class Board {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.width = canvas.width;
    this.height = canvas.height;

    this.snake = new Snake();
    this.apples = [new Apple()];
    this.score = new Score();
  }

  draw() {
    ctx.fillStyle = "black";
    ctx.fillRect(this.x, this.y, this.width, this.height);
    this.apples.forEach((apple) => {
      apple.draw();
    });
    this.snake.draw();
    this.score.draw();
  }

  checkCollision() {
    this.apples.forEach((apple) => {
      if (
        this.snake.x < apple.x + apple.width &&
        this.snake.x + this.snake.width > apple.x &&
        this.snake.y < apple.y + apple.height &&
        this.snake.y + this.snake.height > apple.y
      ) {
        this.snake.tail.push({ x: this.snake.x, y: this.snake.y });
        this.snake.x = apple.x;
        this.snake.y = apple.y;
        this.apples = [new Apple()];
        this.score.score += 10;
      }
    });
  }

  checkCollisionWithWall() {
    if (
      this.snake.x < 0 ||
      this.snake.x+this.snake.width > this.width ||
      this.snake.y < 0 ||
      this.snake.y+this.snake.height > this.height
    ) {
      clearInterval(intervalId);
      alert("Game Over");
      window.location.reload();
    }
  }
}

class Score {
  constructor() {
    this.score = 0;
  }

  draw() {
    ctx.fillStyle = "white";
    ctx.font = "30px Arial";
    ctx.fillText("Score: " + this.score, 10, 30);
  }

}

class Apple {
  constructor() {
    this.x = Math.floor(Math.random() * 40) * 16;
    this.y = Math.floor(Math.random() * 30) * 16;
    this.width = 16;
    this.height = 16;
  }

  draw() {
    ctx.fillStyle = "red";
    ctx.fillRect(this.x, this.y, this.width, this.height);
  }

}

class Snake {
  constructor() {
    this.x = 12 * 16;
    this.y = 10 * 16;
    this.width = 16;
    this.height = 16;

    this.direction = "right";

    this.tail = [];
    this.head = { x: this.x, y: this.y };
  }

  draw() {
    ctx.fillStyle = "white";
    ctx.fillRect(this.x, this.y, this.width, this.height);
    this.tail.forEach((block) => {
      ctx.fillRect(block.x, block.y, this.width, this.height);
    });
  }

  move() {
    this.tail.push({ x: this.x, y: this.y });
    this.delta_move();

    this.head = { x: this.x, y: this.y };
    this.tail.shift();

    if (this.tail.length > 0) {
      this.tail.forEach((block) => {
        if (this.head.x === block.x && this.head.y === block.y) {
          clearInterval(intervalId);
          alert("Game Over");
          window.location.reload();
        }
      });
    }
  }

  delta_move() {
    switch (this.direction) {
      case "up":
        this.y -= this.height;
        break;
      case "down":
        this.y += this.height;
        break;
      case "left":
        this.x -= this.width;
        break;
      case "right":
        this.x += this.width;
        break;
    }
  }
}

// Initialize the game

let board = new Board();
// Game Loop

window.addEventListener("keydown", (e) => {
  switch (e.key) {
    case "ArrowUp":
      if (board.snake.direction !== "down") {
        board.snake.direction = "up";
      }
      break;
    case "ArrowDown":
      if (board.snake.direction !== "up") {
        board.snake.direction = "down";
      }
      break;
    case "ArrowLeft":
      if (board.snake.direction !== "right") {
        board.snake.direction = "left";
      }
      break;
    case "ArrowRight":
      if (board.snake.direction !== "left") {
        board.snake.direction = "right";
      }
      break;
  }
});

function gameLoop() {
  // read input

  // Update display
  board.checkCollision();
  board.snake.move();
  board.checkCollisionWithWall();

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  board.draw();
}

function startGame() {
  intervalId = setInterval(gameLoop, 125);
}